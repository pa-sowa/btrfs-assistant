#ifndef FILEBROWSER_H
#define FILEBROWSER_H

#include "util/Snapper.h"

#include <QDialog>
#include <QFileSystemModel>
#include <QTreeView>

namespace Ui {
class FileBrowser;
}

/**
 * @brief The FileBrowser class that handles the snapshot file browsing dialog
 */
class FileBrowser : public QDialog {
    Q_OBJECT

  public:
    FileBrowser(Btrfs *btrfs, Snapper *snapper, const QString &configName, const QString &rootPath, uint number, const QString &uuid,
                QWidget *parent = nullptr);
    ~FileBrowser();

  private:
    Ui::FileBrowser *m_ui = nullptr;
    QString m_configName;
    QString m_rootPath;
    uint m_number = 0;
    QString m_uuid;
    Btrfs *m_btrfs = nullptr;
    Snapper *m_snapper = nullptr;
    QTreeView *m_treeView = nullptr;
    QFileSystemModel *m_fileModel = nullptr;

    QString selectedPath() const;
    QString selectedRelativePath() const;
    void findSnapshotsWithPath(const QString &path, bool matching);
    void deleteSelectedFile(bool fromAll);

    // TODO: move to Snapper
    QVector<SnapperSubvolume> findSnapshotsWithPath(const QString &path);

  private slots:
    void on_pushButton_close_clicked();
    void on_pushButton_diff_clicked();
    void on_pushButton_restore_clicked();
    void on_treeView_file_customContextMenuRequested(const QPoint &pos);
    void on_findAction_triggered();
    void on_findWithoutAction_triggered();
};

#endif // FILEBROWSER_H
