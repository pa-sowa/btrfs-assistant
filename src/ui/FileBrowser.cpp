#include "FileBrowser.h"
#include "DiffViewer.h"
#include "ui_FileBrowser.h"

#include <QDebug>
#include <QDir>
#include <QMenu>
#include <QMessageBox>

FileBrowser::FileBrowser(Btrfs *btrfs, Snapper *snapper, const QString &configName, const QString &rootPath, uint number,
                         const QString &uuid, QWidget *parent)
    : QDialog(parent), m_ui(new Ui::FileBrowser), m_configName(configName), m_rootPath(rootPath), m_number(number), m_uuid(uuid),
      m_btrfs(btrfs), m_snapper(snapper)
{
    enum Column { NameColumn, SizeColumn, TypeColumn, TimeColumn };

    m_ui->setupUi(this);

    m_treeView = m_ui->treeView_file;

    // Setup the file browser tree view
    m_fileModel = new QFileSystemModel;
    m_fileModel->setRootPath(rootPath);
    m_fileModel->setFilter(QDir::Hidden | QDir::AllEntries | QDir::NoDotAndDotDot);
    // No need to watch for changes of a readonly subvolume
    m_fileModel->setOption(QFileSystemModel::DontWatchForChanges);
    // Whenever new data is loaded resize columns but allow the user to shrink manually if needed
    connect(m_fileModel, &QFileSystemModel::directoryLoaded, this, [this]() {
        for (int columnCount = m_fileModel->columnCount(), c = 0; c < columnCount; ++c) {
            m_treeView->resizeColumnToContents(c);
        }
    });

    m_treeView->setModel(m_fileModel);
    m_treeView->setRootIndex(m_fileModel->index(rootPath));
    m_treeView->hideColumn(TypeColumn);
    m_treeView->sortByColumn(0, Qt::AscendingOrder);
    m_treeView->setContextMenuPolicy(Qt::CustomContextMenu);

    this->setWindowTitle(tr("Snapshot File Viewer"));
}

FileBrowser::~FileBrowser() { delete m_ui; }

QString FileBrowser::selectedPath() const
{
    const QModelIndexList &indexes = m_treeView->selectionModel()->selectedRows();
    if (indexes.isEmpty()) {
        return "";
    } else {
        return m_fileModel->filePath(indexes.first());
    }
}

QString FileBrowser::selectedRelativePath() const { return selectedPath().mid(m_rootPath.size() + 1); }

void FileBrowser::findSnapshotsWithPath(const QString &path, bool matching)
{
    assert(!path.isEmpty());

    const QVector<SnapperSubvolume> subvols = m_snapper->subvols(m_configName);
    if (subvols.size() < 2) {
        QMessageBox::warning(this, tr("Btrfs Assistant"), tr("At least two snapshots are required"));
        return;
    }

    const QString relativeFilePath = path.mid(m_rootPath.size() + 1);
    const QVector<SnapperSubvolume> matchingSnapshots = findSnapshotsWithPath(relativeFilePath);

    QMessageBox::information(
        this, tr("Btrfs Assistant"),
        matching ? tr("This file was found in %1 of %2 snapshots").arg(matchingSnapshots.size()).arg(subvols.size())
                 : tr("This file is missing in %1 of %2 snapshots").arg(subvols.size() - matchingSnapshots.size()).arg(subvols.size()));
}

void FileBrowser::deleteSelectedFile(bool fromAll)
{
    const QString path = selectedRelativePath();
    QVector<SnapperSubvolume> subvols = findSnapshotsWithPath(path);
    QString message;

    if (fromAll) {
        message = tr("Are you sure you want to delete %1 from <strong>all (%2) snapshots</strong>?").arg(path).arg(subvols.size());
    } else {
        message = tr("Are you sure you want to delete %1?").arg(path);
    }

    if (QMessageBox::question(this, tr("Btrfs Assistant"), message) == QMessageBox::No) {
        return;
    }

    m_fileModel->setOption(QFileSystemModel::DontWatchForChanges, false);

    const bool isDir = QFileInfo(selectedPath()).isDir();
    if (!fromAll) {
        for (const SnapperSubvolume &s : subvols) {
            if (s.snapshotNum == m_number) {
                subvols.clear();
                subvols.append(s);
                break;
            }
        }
    }

    const QString mountpoint = QDir::cleanPath(m_btrfs->mountRoot(m_uuid));
    QVector<SnapperSubvolume> failed;

    auto remove = [isDir](const QString &absolutePath) -> bool {
        bool ok = false;
        if (isDir) {
            ok = QDir(absolutePath).removeRecursively();
            if (!ok) {
                qWarning() << "Cannot recursively remove directory" << absolutePath;
            }
        } else {
            ok = QFile::remove(absolutePath);
            if (!ok) {
                qWarning() << "Cannot remove file" << absolutePath;
            }
        }
        return ok;
    };

    for (const SnapperSubvolume &s : subvols) {
        const QString subvolPath = mountpoint + QDir::separator() + s.subvol;
        const QString absolutePath = subvolPath + QDir::separator() + path;
        bool isReadOnly = Btrfs::isSubvolumeReadOnly(subvolPath);
        bool ok = false;
        if (isReadOnly) {
            if (Btrfs::setSubvolumeReadOnly(subvolPath, false)) {
                ok = remove(absolutePath);
                Btrfs::setSubvolumeReadOnly(subvolPath, true);
            } else {
                qWarning() << "Cannot clear read-only flag of" << subvolPath;
            }
        } else {
            ok = remove(absolutePath);
        }

        if (!ok) {
            failed.append(s);
        }
    }

    if (failed.isEmpty()) {
        QMessageBox::information(0, tr("Btrfs Assistant"), tr("Removed successfully"));
    } else {
        QStringList numbers;
        std::transform(failed.begin(), failed.end(), std::back_inserter(numbers),
                       [](const SnapperSubvolume &s) { return QString::number(s.snapshotNum); });
        QMessageBox::critical(0, tr("Btrfs Assistant"),
                              tr("Failed to remove from %1 snapshot(s): %2").arg(failed.size()).arg(numbers.join(", ")));
    }
}

QVector<SnapperSubvolume> FileBrowser::findSnapshotsWithPath(const QString &path)
{
    const QVector<SnapperSubvolume> subvols = m_snapper->subvols(m_configName);
    const QString mountpoint = QDir::cleanPath(m_btrfs->mountRoot(m_uuid));

    QVector<SnapperSubvolume> ret;
    for (const SnapperSubvolume &subvolume : subvols) {
        const QString subvolPath = mountpoint + QDir::separator() + subvolume.subvol;
        if (QFile::exists(subvolPath + QDir::separator() + path)) {
            ret.append(subvolume);
        }
    }
    return ret;
}

void FileBrowser::on_pushButton_close_clicked() { this->close(); }

void FileBrowser::on_pushButton_diff_clicked()
{
    // Get the selected row and ensure it isn't empty
    QModelIndexList indexes = m_treeView->selectionModel()->selectedIndexes();
    if (indexes.count() == 0) {
        return;
    }

    // Check to be sure it isn't a directory
    if (m_fileModel->isDir(indexes.at(0))) {
        QMessageBox::information(this, tr("Diff File"),
                                 m_fileModel->fileName(indexes.at(0)) + tr(" is a directory, only files can be diffed"));
        return;
    }

    // Grad the path of the selected file
    const QString filePath = m_fileModel->filePath(indexes.at(0));

    // Create DiffViewer dialog
    DiffViewer df(m_snapper, m_rootPath, filePath, m_uuid);
    df.exec();
}

void FileBrowser::on_pushButton_restore_clicked()
{
    // Get the selected row and ensure it isn't empty
    QModelIndexList indexes = m_treeView->selectionModel()->selectedIndexes();
    if (indexes.count() == 0) {
        return;
    }

    // Check to be sure it isn't a directory
    if (m_fileModel->isDir(indexes.at(0))) {
        QMessageBox::information(this, tr("Restore File"),
                                 m_fileModel->fileName(indexes.at(0)) + tr(" is a directory, only files can be restored"));
        return;
    }

    if (QMessageBox::question(0, tr("Confirm"), tr("Are you sure you want to restore this the file over the current file?")) !=
        QMessageBox::Yes) {
        return;
    }

    // Restore the file
    const QString filePath = m_fileModel->filePath(indexes.at(0));

    const QString targetPath = m_snapper->findTargetPath(m_rootPath, filePath, m_uuid);

    if (targetPath.isEmpty() || !m_snapper->restoreFile(filePath, targetPath)) {
        QMessageBox::warning(this, tr("Restore Failed"), tr("The file failed to restore"));
        return;
    }

    QMessageBox::information(this, tr("Restore File"), tr("The file was succesfully restored"));
}

void FileBrowser::on_treeView_file_customContextMenuRequested(const QPoint &pos)
{
    const QModelIndexList &indexes = m_treeView->selectionModel()->selectedIndexes();
    if (indexes.isEmpty()) {
        return;
    }

    QMenu menu;
    menu.addAction(tr("&Find all snapshots with this file"), this, &FileBrowser::on_findAction_triggered);
    menu.addAction(tr("Find all snapshots &without this file"), this, &FileBrowser::on_findWithoutAction_triggered);

    menu.addAction(tr("&Delete"), this, [this]() { deleteSelectedFile(false); });

    menu.addAction(tr("Delete from &all snapshots"), this, [this]() { deleteSelectedFile(true); });

    menu.exec(m_ui->treeView_file->mapToGlobal(pos));
}

void FileBrowser::on_findAction_triggered() { findSnapshotsWithPath(selectedPath(), true); }

void FileBrowser::on_findWithoutAction_triggered() { findSnapshotsWithPath(selectedPath(), false); }
