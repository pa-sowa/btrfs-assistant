#!/bin/bash
# This is a very basic script to make a .deb package

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
PROJECT_DIR="$SCRIPT_DIR/.."

NAME=btrfs-assistant
VERSION=`cat "$ PROJECT_DIR/CMakeLists.txt" | grep 'project(' | awk -F' ' '{ print $3 }'`
ARCH=amd64
MAINTAINER="Adam Sowa <pa.sowa.dev@gmail.com>"
DIST_FOLDER=deb

echo "Building version $VERSION"

cmake -B build -S . -DCMAKE_INSTALL_PREFIX=$PWD/$DIST_FOLDER -DCMAKE_BUILD_TYPE='Release'
make -C build -j$(nproc)
make -C build install

mkdir -p $DIST_FOLDER/DEBIAN
cat << EOF > $DIST_FOLDER/DEBIAN/control
Package: $NAME
Version: $VERSION
Architecture: $ARCH
Maintainer: $MAINTAINER
Depends: libbtrfsutil1, libqt5core5a, libqt5gui5, libqt5widgets5
Description: A GUI management tool for Btrfs filesystems.
EOF

dpkg-deb --build $DIST_FOLDER
mv "$DIST_FOLDER.deb" "${NAME}_${VERSION}_${ARCH}.deb"
rm -rf $DIST_FOLDER
